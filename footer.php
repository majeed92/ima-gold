<footer class="site-footer">
    <div class="max-container">
        <div class="flex flex-justify-center flex-align-center">

            <div class="social_links">
                <ul class="social-menu">
                    <li class="fb">
                        <a href="https://www.facebook.com/imaimonetaryadvisory/" target="_blank"></a>
                    </li>
                    <li class="twitter">
                        <a href="https://twitter.com/I_M_A_LEO?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"></a>
                    </li>
                    <li class="insta">
                        <a href="https://www.instagram.com/mohammedmansoorkhan/"></a>
                    </li>

                    <li class="linkdin">
                        <a href="https://www.youtube.com/channel/UCe4Y7tFznh5mH9dBm-0EzGA"></a>
                    </li>

                </ul>
            </div>

        </div>
        <div class="journey_markup">
            <!--<div class="say_hello">Say Hello!</div>-->
            <h3>IMA Gold <br> The definitions of purity</h3>
            <p>
                <!--<a href="contact.php" class="btn-link">Get In Touch</a>-->
            </p>
        </div>
        <div class="copy-right">© Copyright 2019. IMA Gold. All Rights Reserved.</div>
    </div>
</footer>
<div class="siteheader-menu">
    <span class="siteheader-toggle"></span>
    <span class="siteheader-close">Close</span>
    <span class="siteheader-open">Menu</span>
    <div class="menu-content">
        <ul class="mobile-nav flex flex-justify-evenly">
            <li>
                <a href="index.php">
                    <span>Home</span>
                </a>

            </li>
           <!-- <li>
     <a href="about.php">
         <span>About</span>
     </a>
 </li>-->
            <li>
                <a href="services.php">
                    <span>Services</span>
                </a>
            </li>
            <li>
                <a href="faqs.php">
                    <span>FAQ'S</span>
                </a>
                <!--  <ul class="sub-menu">
                    <li><a href="work.php">Case Studies</a></li>
                </ul>-->
            </li>
           
            <li>
                <a href="contact.php">
                    <span>Contact</span>
                </a>
               
            </li>
        </ul>
        <div class="bottom_menu">
            <div class="quick-title">Quick Contact</div>
            <div class="quick-cont">
                <a href="mailto:info@imonetaryadvisory.com">info@imonetaryadvisory.com</a> |
                <a href="tel:919878977771">+91.987.897.7771</a>
            </div>
            <ul class="social-menu">
                <li class="fb">
                    <a href="https://www.facebook.com/imaimonetaryadvisory/" target="_blank"></a>
                </li>
                <li class="twitter">
                    <a href="https://twitter.com/I_M_A_LEO?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"></a>
                </li>
                <li class="insta">
                    <a href="https://www.instagram.com/mohammedmansoorkhan/"></a>
                </li>

                <li class="linkdin">
                    <a href="https://www.youtube.com/channel/UCe4Y7tFznh5mH9dBm-0EzGA"></a>
                </li>
            </ul>
        </div>
    </div>
</div>
