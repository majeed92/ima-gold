<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <title>Web Design Agency | Web Development | IMA Gold</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="icon" type="image/png" href="images/favicon.png">

    <!--       <link rel='stylesheet' href='css/flickity.min.css'>-->
    <!--<link rel="stylesheet" href="css/style.min.css">-->
    <link rel="stylesheet" href="css/home.min.css">
    <link rel="stylesheet" href="css/blue-theme.css">
    <!--    <link rel="stylesheet" href="css/landing.min.css">  -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet"> -->

    <script type="text/javascript">
        var my_image = new Image();
        my_image.src = 'images/lp/clouds.png';

    </script>
    <style>
        * {
            box-sizing: border-box;
        }

        .carousel {
            background: transparent;
        }

        .carousel-cell {
            width: 100%;
            height: 90vh;
            margin-right: 10px;
            background: transparent;
            border-radius: 5px;
            counter-increment: carousel-cell;
        }

        .progress-bar {
            height: 100vh;
            width: 400%;
            position: fixed;
            top: 0;
            background: #333;
            z-index: -1;
            background-size: cover !important;
            -webkit-animation: left-marquee 200s linear infinite 0.5s normal;
            -moz-animation: left-marquee 200s linear infinite 0.5s normal;
            animation: left-marquee 200s linear infinite 0.5s normal;
            background: url('images/lp/clouds.png');

        }

        /*.theme1 .progress-bar{background: url('images/lp/clouds.png');}
		.theme2 .progress-bar{background: url('images/lp/clouds-01.png');}*/


        @-webkit-keyframes left-marquee {
            from {
                bacgkround-position: 0 0;
            }

            to {
                background-position: -4000px 0;
            }

        }

        @-moz-keyframes left-marquee {
            from {
                bacgkround-position: 0 0;
            }

            to {
                background-position: -4000px 0;
            }

        }

        @keyframes left-marquee {
            from {
                bacgkround-position: 0 0;
            }

            to {
                background-position: -4000px 0;
            }

        }

        .progress-bar1 {
            height: 100vh;
            width: 300%;
            position: fixed;
            top: 0;
            background: #333;
            z-index: -1;
            background-size: cover !important;
        }

        .theme1 .progress-bar1 {
            background: url('images/lp/sea.jpg') no-repeat left bottom;
        }

        .theme2 .progress-bar1 {
            background: url('images/lp/sea-01.jpg') no-repeat left bottom;
        }

        .baloon {
            height: 100vh;
            width: 300%;
            position: fixed;
            top: 0;
            z-index: -1;
        }


        .flickity-page-dots {
            bottom: -40px;
        }

        .theme1 .flickity-slider {
            cursor: url('images/cursor.png'), auto;
        }

        .theme2 .flickity-slider {
            cursor: url('images/cursor-01.png'), auto;
        }

        @media (max-width: 991px) {
            .progress-bar1 {
                width: 900%;
                background-size: auto 100%;
            }

            .progress-bar {
                width: 1200%;
                background-size: auto 100%;
            }

            .carousel-cell {
                background-size: 30%;
            }
        }

        @media (max-width: 767px) {
            .progress-bar1 {
                width: 900%;
            }

            .progress-bar {
                width: 1200%;
            }
        }

    </style>
    <style>
        .dot,
        .icon {
            text-align: center
        }

        .main-loader {
            position: fixed;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            background: #262729;
            /* background: #a72453;
            background: -moz-linear-gradient(top, #a72453 0, #79367f 63%, #79367f 100%);
            background: -webkit-linear-gradient(top, #a72453 0, #79367f 63%, #79367f 100%);
            background: linear-gradient(to bottom, #a72453 0, #79367f 63%, #79367f 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a72453', endColorstr='#79367f', GradientType=0); */
            z-index: 99999;
        }

        .screen-overlay,
        body {
            background: #000
        }

        .icon {
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%);
            display: block;
            margin: 0 auto;
            left: 50%
        }

        .screen-overlay {
            z-index: 998;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            position: fixed;
            transition: all ease .5s
        }

        .loaded .screen-overlay {
            transition-delay: .5s;
            opacity: 0;
            z-index: -998
        }

        .outer-layer {
            width: 402px;
            height: 114px;
            position: relative;
            margin: 0 auto
        }

        .dot,
        .image-load {
            left: 0;
            position: absolute
        }

        @-webkit-keyframes SkLgXTFYahz_HJRA_JEpG_Animation {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }

            6.67% {
                -webkit-transform: rotate(-8deg);
                transform: rotate(-8deg)
            }

            100%,
            13.33% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }
        }

        @keyframes SkLgXTFYahz_HJRA_JEpG_Animation {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }

            6.67% {
                -webkit-transform: rotate(-8deg);
                transform: rotate(-8deg)
            }

            100%,
            13.33% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }
        }

        @-webkit-keyframes SkLgXTFYahz_Hyy3OyEaM_Animation {

            0%,
            13.33%,
            20% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            26.67% {
                -webkit-transform: scale(1.1, 1.1);
                transform: scale(1.1, 1.1)
            }

            33.33%,
            40% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            46.67% {
                -webkit-transform: scale(1.1, 1.1);
                transform: scale(1.1, 1.1)
            }

            53.33%,
            60% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            66.67% {
                -webkit-transform: scale(1.1, 1.1);
                transform: scale(1.1, 1.1)
            }

            100%,
            73.33%,
            80% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }
        }

        @keyframes SkLgXTFYahz_Hyy3OyEaM_Animation {

            0%,
            13.33%,
            20% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            26.67% {
                -webkit-transform: scale(1.1, 1.1);
                transform: scale(1.1, 1.1)
            }

            33.33%,
            40% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            46.67% {
                -webkit-transform: scale(1.1, 1.1);
                transform: scale(1.1, 1.1)
            }

            53.33%,
            60% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            66.67% {
                -webkit-transform: scale(1.1, 1.1);
                transform: scale(1.1, 1.1)
            }

            100%,
            73.33%,
            80% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }
        }

        @-webkit-keyframes SkLgXTFYahz_r1RiuJVaG_Animation {
            0% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            6.67% {
                -webkit-transform: translate(0, -100px);
                transform: translate(0, -100px)
            }

            13.33%,
            20% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            26.67% {
                -webkit-transform: translate(-67px, 0);
                transform: translate(-67px, 0)
            }

            33.33%,
            40% {
                -webkit-transform: translate(-134px, 0);
                transform: translate(-134px, 0)
            }

            46.67% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            53.33%,
            60% {
                -webkit-transform: translate(134px, 0);
                transform: translate(134px, 0)
            }

            66.67% {
                -webkit-transform: translate(67px, 0);
                transform: translate(67px, 0)
            }

            100%,
            73.33%,
            80% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }
        }

        @keyframes SkLgXTFYahz_r1RiuJVaG_Animation {
            0% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            6.67% {
                -webkit-transform: translate(0, -100px);
                transform: translate(0, -100px)
            }

            13.33%,
            20% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            26.67% {
                -webkit-transform: translate(-67px, 0);
                transform: translate(-67px, 0)
            }

            33.33%,
            40% {
                -webkit-transform: translate(-134px, 0);
                transform: translate(-134px, 0)
            }

            46.67% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            53.33%,
            60% {
                -webkit-transform: translate(134px, 0);
                transform: translate(134px, 0)
            }

            66.67% {
                -webkit-transform: translate(67px, 0);
                transform: translate(67px, 0)
            }

            100%,
            73.33%,
            80% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }
        }

        @-webkit-keyframes rkXxQTYYTnG_Animation {
            0% {
                opacity: 1
            }

            13.33%,
            93.33% {
                opacity: 0
            }

            100% {
                opacity: 1
            }
        }

        @keyframes rkXxQTYYTnG_Animation {
            0% {
                opacity: 1
            }

            13.33%,
            93.33% {
                opacity: 0
            }

            100% {
                opacity: 1
            }
        }

        @-webkit-keyframes rkOXaYYa3M_BJVcuJNTf_Animation {
            40% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            46.67% {
                -webkit-transform: scale(.9, .9);
                transform: scale(.9, .9)
            }

            53.33%,
            60%,
            80% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            86.67% {
                -webkit-transform: scale(1.1, 1.1);
                transform: scale(1.1, 1.1)
            }

            0%,
            100%,
            93.33% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }
        }

        @keyframes rkOXaYYa3M_BJVcuJNTf_Animation {
            40% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            46.67% {
                -webkit-transform: scale(.9, .9);
                transform: scale(.9, .9)
            }

            53.33%,
            60%,
            80% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            86.67% {
                -webkit-transform: scale(1.1, 1.1);
                transform: scale(1.1, 1.1)
            }

            0%,
            100%,
            93.33% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }
        }

        @-webkit-keyframes rkOXaYYa3M_rJf9u1Eaf_Animation {
            40% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            46.67% {
                -webkit-transform: translate(-134px, 0);
                transform: translate(-134px, 0)
            }

            53.33%,
            60%,
            80% {
                -webkit-transform: translate(-268px, 0);
                transform: translate(-268px, 0)
            }

            86.67% {
                -webkit-transform: translate(-134px, 0);
                transform: translate(-134px, 0)
            }

            0%,
            100%,
            93.33% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }
        }

        @keyframes rkOXaYYa3M_rJf9u1Eaf_Animation {
            40% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            46.67% {
                -webkit-transform: translate(-134px, 0);
                transform: translate(-134px, 0)
            }

            53.33%,
            60%,
            80% {
                -webkit-transform: translate(-268px, 0);
                transform: translate(-268px, 0)
            }

            86.67% {
                -webkit-transform: translate(-134px, 0);
                transform: translate(-134px, 0)
            }

            0%,
            100%,
            93.33% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }
        }

        @-webkit-keyframes rJxXatK6hf_SkoFu1Epf_Animation {
            20% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            26.67% {
                -webkit-transform: scale(.9, .9);
                transform: scale(.9, .9)
            }

            33.33%,
            40%,
            60% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            66.67% {
                -webkit-transform: scale(.9, .9);
                transform: scale(.9, .9)
            }

            73.33%,
            80% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            86.67% {
                -webkit-transform: scale(.9, .9);
                transform: scale(.9, .9)
            }

            0%,
            100%,
            93.33% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }
        }

        @keyframes rJxXatK6hf_SkoFu1Epf_Animation {
            20% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            26.67% {
                -webkit-transform: scale(.9, .9);
                transform: scale(.9, .9)
            }

            33.33%,
            40%,
            60% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            66.67% {
                -webkit-transform: scale(.9, .9);
                transform: scale(.9, .9)
            }

            73.33%,
            80% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }

            86.67% {
                -webkit-transform: scale(.9, .9);
                transform: scale(.9, .9)
            }

            0%,
            100%,
            93.33% {
                -webkit-transform: scale(1, 1);
                transform: scale(1, 1)
            }
        }

        @-webkit-keyframes rJxXatK6hf_rJOt_14pz_Animation {
            20% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            26.67% {
                -webkit-transform: translate(67px, 0);
                transform: translate(67px, 0)
            }

            33.33%,
            40%,
            60% {
                -webkit-transform: translate(134px, 0);
                transform: translate(134px, 0)
            }

            66.67% {
                -webkit-transform: translate(201px, 0);
                transform: translate(201px, 0)
            }

            73.33%,
            80% {
                -webkit-transform: translate(268px, 0);
                transform: translate(268px, 0)
            }

            86.67% {
                -webkit-transform: translate(134px, 0);
                transform: translate(134px, 0)
            }

            0%,
            100%,
            93.33% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }
        }

        @keyframes rJxXatK6hf_rJOt_14pz_Animation {
            20% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }

            26.67% {
                -webkit-transform: translate(67px, 0);
                transform: translate(67px, 0)
            }

            33.33%,
            40%,
            60% {
                -webkit-transform: translate(134px, 0);
                transform: translate(134px, 0)
            }

            66.67% {
                -webkit-transform: translate(201px, 0);
                transform: translate(201px, 0)
            }

            73.33%,
            80% {
                -webkit-transform: translate(268px, 0);
                transform: translate(268px, 0)
            }

            86.67% {
                -webkit-transform: translate(134px, 0);
                transform: translate(134px, 0)
            }

            0%,
            100%,
            93.33% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0)
            }
        }

        #B1XatYT3G * {
            -webkit-animation-duration: 7.5s;
            animation-duration: 7.5s;
            -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
            -webkit-animation-timing-function: cubic-bezier(0, 0, 1, 1);
            animation-timing-function: cubic-bezier(0, 0, 1, 1)
        }

        #rJxXatK6hf_rJOt_14pz {
            -webkit-animation-name: rJxXatK6hf_rJOt_14pz_Animation;
            animation-name: rJxXatK6hf_rJOt_14pz_Animation;
            -webkit-transform-origin: 50% 50%;
            transform-origin: 50% 50%;
            transform-box: fill-box;
            -webkit-transform: translate(0, 0);
            transform: translate(0, 0)
        }

        #rJxXatK6hf_SkoFu1Epf,
        #rkOXaYYa3M_rJf9u1Eaf {
            -webkit-transform-origin: 50% 50%;
            transform-box: fill-box
        }

        #rJxXatK6hf_SkoFu1Epf {
            -webkit-animation-name: rJxXatK6hf_SkoFu1Epf_Animation;
            animation-name: rJxXatK6hf_SkoFu1Epf_Animation;
            transform-origin: 50% 50%;
            -webkit-transform: scale(1, 1);
            transform: scale(1, 1)
        }

        #rkOXaYYa3M_rJf9u1Eaf {
            -webkit-animation-name: rkOXaYYa3M_rJf9u1Eaf_Animation;
            animation-name: rkOXaYYa3M_rJf9u1Eaf_Animation;
            transform-origin: 50% 50%;
            -webkit-transform: translate(0, 0);
            transform: translate(0, 0)
        }

        #rkOXaYYa3M_BJVcuJNTf {
            -webkit-animation-name: rkOXaYYa3M_BJVcuJNTf_Animation;
            animation-name: rkOXaYYa3M_BJVcuJNTf_Animation;
            -webkit-transform-origin: 50% 50%;
            transform-origin: 50% 50%;
            transform-box: fill-box;
            -webkit-transform: scale(1, 1);
            transform: scale(1, 1)
        }

        #rkXxQTYYTnG {
            -webkit-animation-name: rkXxQTYYTnG_Animation;
            animation-name: rkXxQTYYTnG_Animation;
            -webkit-transform-origin: 50% 50%;
            transform-origin: 50% 50%;
            transform-box: fill-box;
            opacity: 1
        }

        #SkLgXTFYahz_Hyy3OyEaM,
        #SkLgXTFYahz_r1RiuJVaG {
            -webkit-transform-origin: 50% 50%;
            transform-box: fill-box
        }

        #SkLgXTFYahz_r1RiuJVaG {
            -webkit-animation-name: SkLgXTFYahz_r1RiuJVaG_Animation;
            animation-name: SkLgXTFYahz_r1RiuJVaG_Animation;
            transform-origin: 50% 50%;
            -webkit-transform: translate(0, 0);
            transform: translate(0, 0)
        }

        #SkLgXTFYahz_Hyy3OyEaM {
            -webkit-animation-name: SkLgXTFYahz_Hyy3OyEaM_Animation;
            animation-name: SkLgXTFYahz_Hyy3OyEaM_Animation;
            transform-origin: 50% 50%;
            -webkit-transform: scale(1, 1);
            transform: scale(1, 1)
        }

        #SkLgXTFYahz_HJRA_JEpG {
            -webkit-animation-name: SkLgXTFYahz_HJRA_JEpG_Animation;
            animation-name: SkLgXTFYahz_HJRA_JEpG_Animation;
            -webkit-transform-origin: 50% 50%;
            transform-origin: 50% 50%;
            transform-box: fill-box;
            -webkit-transform: rotate(0);
            transform: rotate(0)
        }

        .image-load {
            max-width: 134px;
            padding: 0 10px;
            top: 0;
            box-sizing: border-box
        }

        img {
            max-width: 100%
        }

        .dot {
            right: 0;
            top: 92px
        }

        div#B1XatYT3G {
            position: relative
        }

        .image-load:nth-child(2) {
            left: 268px
        }

        .image-load:nth-child(4) {
            left: 0;
            right: 0;
            margin: 0 auto
        }

        .image-load {
            width: 134px;
        }

        @media (max-width: 767px) {
            .outer-layer {
                transform: scale(0.7);
            }
        }

    </style>

</head>

<body style="width: 100%; overflow: hidden;">


    <?php include 'header.php';?>
    <div class="carousel full_height">
        <!--<div id="marquee"></div>-->
        <div class="carousel-cell cases-items__wrapper">
            <article class="flex flex-align-center flex-justify-center">
                <div class="case-item__content js-case-content">
                    <h1 class="case-item__title">Branding
                        <br>And Identity</h1>
                    <p style="opacity:1;" class="case-item__description">
                        <span>Your first impression is your last impression. You know it's
                            <br>true. So let us build you an identity your customers will
                            <br>take notice of.</span>
                    </p>
                    <div class="bottom_points mobile" style="opacity:1; visibility:visible;">
                        <a href="services.php" class="link_slider">Explore Services</a>
                    </div>
                </div>
                <div class="bottom_points desktop" style="opacity:1; visibility:visible;">
                    <a href="services.php" class="link_slider">Explore Services</a>
                </div>
            </article>
        </div>
        <div class="carousel-cell cases-items__wrapper">
            <article class="flex flex-align-center flex-justify-center">
                <div class="case-item__content js-case-content">
                    <h1 class="case-item__title">Quality
                        <br>And Passion</h1>
                    <p style="opacity:1;" class="case-item__description">
                        <span>If a picture is worth a thousand words, then our portfolio
                            <br>collection speaks volumes. Check out our work.</span>
                    </p>
                    <div class="bottom_points mobile" style="opacity:1; visibility:visible;">
                        <a href="work.php" class="link_slider">Pursue Our Work</a>
                    </div>
                </div>
                <div class="bottom_points desktop" style="opacity:1; visibility:visible;">
                    <a href="work.php" class="link_slider">Pursue Our Work</a>
                </div>
            </article>
        </div>
        <div class="carousel-cell cases-items__wrapper">
            <article class="flex flex-align-center flex-justify-center">
                <div class="case-item__content js-case-content">
                    <h1 class="case-item__title">Your Digital Marketing Team</h1>
                    <p style="opacity:1;" class="case-item__description">
                        <span>We promise to provide our customers with the best experience from the beginning of a project to its
                            completion.
                        </span>
                    </p>
                    <div class="bottom_points mobile" style="opacity:1; visibility:visible;">
                        <a href="index.php" class="link_slider">About Studio</a>
                    </div>
                </div>
                <div class="bottom_points desktop" style="opacity:1; visibility:visible;">
                    <a href="index.php" class="link_slider">About Studio</a>
                </div>
            </article>
        </div>
    </div>

    <div class="progress-bar1"></div>
    <div class="baloon" style="background-image: url('images/lp/ballon.png'); background-position: right top 20%; background-repeat: no-repeat;"></div>
    <div class="progress-bar"></div>

    <div class="siteheader-menu">
        <span class="siteheader-toggle"></span>
        <span class="siteheader-close">Close</span>
        <span class="siteheader-open">Menu</span>
        <div class="menu-content">
            <ul class="mobile-nav flex flex-justify-evenly">
                <li>
                    <a href="services.php">What we do
                        <span>services</span>
                        <span class="small">Pure passion. Stunning success.</span>
                    </a>
                </li>
                <li>
                    <a href="work.php">What we love
                        <span>work</span>
                        <span class="small">Beauty. Brains. Brawn.</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="work.php">Case Studies</a></li>
                        <li><a href="work-portfolio.php">Portfolio</a></li>
                    </ul>
                </li>
                <li>
                    <a href="index.php">About us
                        <span>studio</span>
                        <span class="small">Design agency. Extraordinaire.</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="index.php">About</a>
                        </li>
                        <!--<li>
                                <a href="blog-list.php">Design Journal</a>
                            </li>-->

                    </ul>
                </li>
                <li>
                    <a href="contact.php">Let’s begin!
                        <span>hello</span>
                        <span class="small">Your journey starts here.</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="contact.php">Quick Contact</a></li>
                        <li><a href="#" target="_blank">Creative Brief</a></li>
                    </ul>
                </li>
            </ul>
            <div class="bottom_menu">
                <div class="quick-title">Quick Contact</div>
                <div class="quick-cont">
                    <a href="mailto:info@imonetaryadvisory.com">info@imonetaryadvisory.com</a> |
                    <a href="tel:919878977771">+91.987.897.7771</a>
                </div>
                <ul class="social-menu">
                    <li class="fb">
                        <a href="https://www.facebook.com/imaimonetaryadvisory/" target="_blank"></a>
                    </li>
                    <li class="twitter">
                        <a href="https://twitter.com/I_M_A_LEO?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"></a>
                    </li>
                    <li class="insta">
                        <a href="https://www.instagram.com/mohammedmansoorkhan/"></a>
                    </li>
                    <li class="pin">
                        <a href="#"></a>
                    </li>
                    <li class="linkdin">
                        <a href="https://www.youtube.com/channel/UCe4Y7tFznh5mH9dBm-0EzGA"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <script src='js/flickity.pkgd.min.js'></script>



    <!--<script src="js/index.js"></script>-->
    <script src="js/jquery.min.js"></script>
    <!-- <script src="js/shared.js"></script>-->





    <script>
        var STATE = {
            'ui': 'dark',

            'common': {
                'visible': true,
                'staticHeader': false
            },
            'background': {
                'id': 'case-slide-1',
                'effect': 1
            },
            'case': {
                // Active case index
                'index': 0,

                // Case count
                'count': 3,

                // Navigation is visible
                'navigation': false
            }
        };

    </script>
    <!--    <script src="js/landing.js"></script>-->

    <script>
        jQuery(document).ready(function($) {


            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;


            jQuery('.siteheader-toggle').click(function(event) {
                jQuery('.siteheader-menu').toggleClass('open_menu');
                if (jQuery('.siteheader-menu').hasClass('open_menu') == true) {
                    jQuery('.siteheader-menu').height(((h * 88) / 100).toFixed(0));
                } else {
                    jQuery('.siteheader-menu').height(65);
                }
            });

        });

        /** -- **/

        var width = 100,
            perfData = window.performance.timing, // The PerformanceTiming interface represents timing-related performance information for the given page.
            EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart),
            time = parseInt((EstimatedTime / 1000) % 60) * 80;

        // Percentage Increment Animation
        var PercentageID = $("#precent"),
            start = 0,
            end = 100,
            durataion = time;
        animateValue(PercentageID, start, end, durataion);

        function animateValue(id, start, end, duration) {

            var range = end - start,
                current = start,
                increment = end > start ? 1 : -1,
                stepTime = Math.abs(Math.floor(duration / range)),
                obj = $(id);

            var timer = setInterval(function() {
                current += increment;
                $(obj).text(current + "%");
                //obj.innerHTML = current;
                if (current == end) {
                    clearInterval(timer);
                }
            }, stepTime);
        }

    </script>
    <script src="js/home-scripts.js"></script>


    <!-- <script src="js/TweenMax.min.js"></script>
    <script src="js/MorphSVGPlugin.min.js"></script> -->
    <script>

    </script>

    <script>
        $(document).ready(function() {
            var d = new Date();
            d = d.getDate();

            //console.log(d);
            // As a side note, this === el.
            if (d % 2 === 0) {
                $('body').addClass('theme2');
                $('body').removeClass('theme1');
            } else {
                $('body').removeClass('theme2');
                $('body').addClass('theme1');
            }

        });

    </script>
</body>

</html>
