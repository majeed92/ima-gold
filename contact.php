<!doctype html>
<html class="no-js is-not-ie-edge has-native-scroll">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Contact | IMA Gold</title>
    <meta name="keywords" content="">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
    <!-- <link rel="stylesheet" href="../use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">-->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/style.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/blue-theme.css">

    <script>
        document.documentElement.className = document.documentElement.className.replace('no-js', 'js')

    </script>

    <script src="js/jquery.min.js"></script>

    <script src="js/validate.js"></script>
    <script src="js/forms-validation.js"></script>


</head>

<body>

    <div class="loader">
        <div class="loading"></div>
    </div>

    <div id="page">
        <?php include 'header.php';?>

        <div class="contact_content">
            <div id="success" class="thanku-modal" style="display: none;">
                <div class="table">
                    <div class="table-cell">
                        <div class="green-thanku-box">
                            <div class="bell-icon"><img src="images/bell-icon.png" alt=""></div>
                            <h2 class="heading"><strong>Thank You!</strong>

                            </h2>
                            <p>A confirmation email is sent to your registered email address. We will get back to you as soon as possible!</p>

                            <a href="index.php" class="btn btn-1 back-to-home-btn">
                                <svg>
                                    <rect x="0" y="0" fill="none" width="100%" height="100%" />
                                </svg>
                                Back to home
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inside_content banner_content">
                <h1 class="page_title">
                    <span>Feel Free</span> to Contact us</h1>
                <p>Want to discuss your feelings? Ready to take things a step further? We’d love to connect with you.</p>
            </div>

            <div class="wrap-content inside_content clearfix">
                <!-- <ul class="flex flex-justify-between flex-align-start contact-info link_info_buttons">
                    <li>
                        <a href="tel:+08045174517">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <span>080 4517 4517</span>
                        </a>
                    </li>
                    <li>
                        <a href="mailto:info@imonetaryadvisory.com">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <span>info@imonetaryadvisory.com</span>
                        </a>
                    </li>
                    <li>
                        <a href="tel:+08043743458">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <span>080 4374 3458</span>
                        </a>
                    </li>
                </ul>-->
                <div class="map-link">
                    <div class="address-link">
                        <h3 class="titlecaps"><span>IMA Gold Shivajinagar</span></h3>
                        <h3><span># 25, Old No. 1/1, Lady Curzon Road, <br>Shivaji Nagar, Bangalore 560001.</span></h3>
                        <p class=""><a class="" href="tel:+080-4374-3458"> <i class="fa fa-phone" aria-hidden="true"></i> &nbsp; 080 4374 3458</a></p>
                        <p class=""><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; <a class="location-view" href="https://goo.gl/maps/E9sC6oS7oNt" target="_blank">Click Here to Get Directions</a></p>
                        <p class=""><i class="fa fa-clock-o" aria-hidden="true"></i> &nbsp; Open : All Days 10:00 am to 7:30 pm</p>

                    </div>

                    <div class="address-link">
                        <h3 class="titlecaps"><span>IMA Gold Yeshwanthpur</span></h3>
                        <h3><span> # 37, 1st Floor, 1st Main Road, <br>Yeshwanthpur, Gokul 1st Stage 4th Phase, <br>Bangalore, 560022.</span></h3>

                        <p class=""><a class="" href="tel:+080-4374-3458"> <i class="fa fa-phone" aria-hidden="true"></i> &nbsp; 080 4374 3458</a></p>
                        <p class=""><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; <a class="location-view" href="https://goo.gl/maps/gnq4GqShprJ2" target="_blank">Click Here to Get Directions</a></p>
                        <p class="slideanim slide"><i class="fa fa-clock-o" aria-hidden="true"></i> &nbsp; Open : All Days 10:00 am to 7:00 pm</p>

                    </div>

                    <div class="address-link">
                        <h3 class="titlecaps"><span>IMA Gold Jayanagar</span></h3>
                        <h3><span> # 343/34, 1st Floor, 32nd 'E' Cross, <br>4th 'T' Block, Jayanagar, Bangalore - 560041</span></h3>
                        <p class=""><a class="" href="tel:+080-4374-3458"> <i class="fa fa-phone" aria-hidden="true"></i> &nbsp; 080 4374 3458</a></p>
                        <p class=""><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; <a class="location-view" href="https://goo.gl/maps/KJD7FNPx1gB2" target="_blank">Click Here to Get Directions</a></p>
                        <p class="slideanim slide"><i class="fa fa-clock-o" aria-hidden="true"></i> &nbsp; Open : All Days 10:00 am to 7:00 pm</p>

                    </div>

                </div>

                <div class="contact-form">

                    <form action="#" method="post" id="contact_form" class="form_field" autocomplete="off">
                        <div class="field flex flex-justify-between ">
                            <div class="half">
                                <div class="field_set">
                                    <input type="text" class="input__field" placeholder="" name="full_name" id="full-name" autocomplete="off" />
                                    <span class="validation-indicator">
                                        <i class="fa fa-check"></i>
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <label class="input__label" for="full-name">Name</label>
                                    <span class="focus-border"></span>
                                </div>
                                <div class="field_set">
                                    <input type="text" class="input__field" placeholder="" name="email_address" id="email-address" autocomplete="off" />
                                    <span class="validation-indicator">
                                        <i class="fa fa-check"></i>
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <label class="input__label" for="email-address">Email</label>
                                    <span class="focus-border"></span>
                                </div>
                                <div class="field_set">
                                    <input type="text" class="input__field" placeholder="" name="company_name" id="company-name" autocomplete="off" />
                                    <span class="validation-indicator">
                                        <i class="fa fa-check"></i>
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <label class="input__label" for="company-name">Company Name</label>
                                    <span class="focus-border"></span>
                                </div>
                                <div class="field_set">
                                    <input type="text" class="input__field" onkeydown="checkCaptcha(this, arguments);" onkeyup="checkCaptcha(this, arguments);" onblur="checkCaptcha(this, arguments);" onfocus="checkCaptcha(this, arguments);" placeholder="" name="captcha" id="captcha" autocomplete="off" />
                                    <span class="validation-indicator">
                                        <i class="fa fa-check"></i>
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <label class="input__label" for="captcha-label" id="captcha-label"></label>
                                    <span class="focus-border"></span>
                                </div>


                            </div>
                            <div class="half">
                                <div class="field_set equal_height">
                                    <input type="hidden" name="formid" value="1">
                                    <textarea class="input__field" placeholder="" name="message" id="message" autocomplete="off"></textarea>
                                    <span class="validation-indicator">
                                        <i class="fa fa-check"></i>
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <label class="input__label" for="message">Message</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                        <div class="field flex flex-align-start flex-justify-start">
                            <button type="button" class="btn btn-1" onclick="formSubmit(this);" id="form_submit">
                                <svg>
                                    <rect x="0" y="0" fill="none" width="100%" height="100%" />
                                </svg>
                                Shoot EMAIL
                            </button>

                            <button type="button" class="btn btn-1" style="display:none;" id="btn_loader">
                                <span class="spinner">
                                    <span class="rect1"></span>
                                    <span class="rect2"></span>
                                    <span class="rect3"></span>
                                </span>
                            </button>


                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include 'footer.php';?>

    </div>



    <script src="js/slick.min.js"></script>
    <script>
        var num1 = Math.floor(Math.random() * 10);
        var num2 = Math.floor(Math.random() * 10);
        var sum = num1 + num2;
        jQuery(document).ready(function($) {



            jQuery('#captcha-label').text("I am not Robot. " + num1 + " + " + num2 + " = ?")

            jQuery('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav'
            });

            jQuery('.slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                focusOnSelect: true
            });

            jQuery('.input__field').on('input', function() {
                _focus();
            })
            jQuery('.input__field').on('focus', function() {
                _focus();
            })

        });

        function checkCaptcha(e, args) {
            if (e.value.length > 0) {
                if (e.value == sum) {
                    jQuery(e).addClass('valid');
                    jQuery(e).removeClass('error');
                } else {
                    jQuery(e).removeClass('valid');
                    jQuery(e).addClass('error');
                }
            }

            return false;

        }


        function formSubmit(x) {

            // jQuery(x).addClass('with-loader').css('display','none');
            jQuery(x).css('display', 'none');
            jQuery('#btn_loader').css('display', 'block');
            setTimeout(function() {
                jQuery("#contact_form").submit();
                jQuery(x).css('display', 'block');
                jQuery('#btn_loader').css('display', 'none');

                // jQuery(x).removeClass('with-loader');
            }, 500);
        }

        function _focus() {
            jQuery('.input__field').each(function(inputEl) {
                // in case the input is already filled..
                if (jQuery(this).val()) {
                    jQuery(this).parents('.field_set').addClass('input--filled');
                    jQuery(this).parents('.field_set').addClass('input--filled');
                } else {
                    jQuery(this).parents('.field_set').removeClass('input--filled');
                    jQuery(this).parents('.field_set').removeClass('input--filled');
                }


            });
        }

    </script>

    <script src="js/scripts.js"></script>
    <script>
        jQuery(document).ready(function($) {
            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;
            jQuery('body, html, #page').height(h);

            jQuery('.siteheader-toggle').click(function(event) {
                jQuery('.siteheader-menu').toggleClass('open_menu');
                if (jQuery('.siteheader-menu').hasClass('open_menu') == true) {
                    jQuery('.siteheader-menu').height(((h * 88) / 100).toFixed(0));
                } else {
                    jQuery('.siteheader-menu').height(65);
                }
            });

        });

        jQuery(window).resize(function($) {
            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;
            jQuery('body, html, #page').height(h);
        });

        jQuery(window).load(function() {
            if (window.location.hash == '#success') {
                $('#success').slideDown();

            }
        });

    </script>

</body>

</html>
