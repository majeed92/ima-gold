<!doctype html>
<html lang="en" class="no-js is-not-ie-edge has-native-scroll">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Home | IMA Gold</title>
    <meta name="keywords" content="">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="fonts/style.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="css/about.css">
    <link rel="stylesheet" href="css/blue-theme.css">
    <script>
        document.documentElement.className = document.documentElement.className.replace('no-js', 'js')

    </script>

    <style>

    </style>

</head>

<body>
    <!-- <div class="loader">
        <div class="loading"></div>
    </div>-->

    <div id="page">
        <?php include 'header.php';?>

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="images/office/11.JPG" alt="Los Angeles" style="width:100%;">
                </div>

                <div class="item">
                    <img src="images/office/12.JPG" alt="Chicago" style="width:100%;">
                </div>

                <div class="item">
                    <img src="images/office/15.JPG" alt="New york" style="width:100%;">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="about_content">

            <!--dsfasdfasdfasdfdsf-->
            <div class="inside_content banner_content animate-content">
                <h1 class="page_title">
                    <span>About</span> IMA Gold.</h1>
                <p>At IMA Gold we always wish the best for our patrons. We invite you to experience IMA Bullion Trading Services Ltd.’s brand new venture where we wish to provide our patrons with a personalized buying and selling experience when it comes to Gold and Silver Coins and Bars.</p>
                <p>IMA Bullion Trading Services Ltd is India’s first and truly revolutionary center that deals purely with Gold and Silver and allows you to invest in precious metals that have stood the test of time.</p>
                <p>Volatility exists in every aspect of a man-made object and this is especially true when it comes to modern-day fiat currencies which are man-made and are not backed by precious metals as in the early days of trade and commerce. IMA Gold offers you a chance to hedge against insecurities of an uncertain future by investing in the evergreen Gold and Silver in unlimited amounts at the lowest prices!</p>
                <p>Physical Coins which consist of G-Coins (Gold Coins) and S-Coins (Silver Coins) is the revolution in the making and will withstand all scrutiny unlike outlawed and legally insecure digital currencies and will forever be the Gold Standard amongst all asset classes.</p>
            </div>
            <!-- excellence-banner start -->
            <div class="excellence-banner animate-content">
                <div class="eb-text">
                    <div class="eb-heading">EXCELLENCE</div>

                    <div class="eb-sub">
                        GUARANTEED
                    </div>
                </div>
                <div class="eb-image">
                    <svg viewBox="0 0 120 120" class="progresss">
                        <circle cx="60" cy="60" r="54" fill="none" stroke="#b3853d" stroke-width="8" class="inner-circle" />
                        <circle cx="60" cy="60" r="54" fill="none" stroke="#b3853d" stroke-width="8" class="outer-circle" />
                    </svg>

                    <img src="images/excellence-seal2.png" alt="">

                </div>
            </div>
            <!-- excellence-banner end -->
            <!--<div class="work-with " id="work-with">
                <div class="inside_content work-with animate-content">
                    <h2>IMA Gold Products &amp; Services</h2>
                    <div class="flex flex-justify-between flex-align-start
                    work-grid">
                        <div class="work-box">
                            <div class="work-head">
                                <h3>
                                    <span>01</span>Interest Free Gold Loan</h3>
                                <span>Eligibility &amp; Criteria</span>
                            </div>
                            <p>Active Membership with IMA</p>
                            <p>Minimum 10gm Gold to be Pledged</p>
                            <p>Maximum 4 Lakhs Worth Gold can be Pledged</p>
                            <p>Loan Validity 365 days</p>
                        </div>
                        <div class="work-box">
                            <div class="work-head">
                                <h3>
                                    <span>02</span>Old Gold Purchase</h3>
                                <span>Eligibility &amp; Criteria</span>
                            </div>
                            <p>Purchase of Old Gold in any condition</p>
                            <p>Free purity testing</p>
                            <p>Personalised services like you've never experienced before</p>
                        </div>
                        <div class="work-box">
                            <div class="work-head">
                                <h3>
                                    <span>03</span>IMA Gold/Silver Coins &amp; Bars</h3>
                                <span>Eligibility &amp; Criteria</span>
                            </div>
                            <p>The only asset class without an expiry date</p>
                            <p>The oldest, safest and strongest asset class known to man. Buy ICoins and IBars and own precious metal that will always be in demand and totally immune from ups and downs of legal tender and outlawed crypto currencies!</p>
                            <p>Availble readily in 1,2,4,5,10,50,100,500 and 1000 gms.</p>
                            <p>Immediate delivery upon NEFT/RTGS confirmation</p>
                            <p>Orders for upto 100 KG of Gold &amp; upto 1000 KG of silver can be fulfilled on daily basis</p>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="work-with " id="work-with">
                <div class="inside_content work-with animate-content">
                    <h2>IMA Gold Products &amp; Services</h2>
                    <div class="flex flex-justify-between flex-align-start
                    work-grid">
                        <div class="work-box">
                            <div class="work-head">
                                <h3>
                                    <span>01</span>Why customer would avail a gold loan with IMAGOLD?</h3>
                            </div>
                            <p>Interest free</p>
                            <p>No Processing fee &amp; No Appraisers fee</p>
                            <p>No preclosure Charges &amp; No Late Payment charges</p>
                            <p>Nominal Security charges for the safe custody &amp; Easy Documentation</p>
                            <p>Transparency in the complete process</p>
                            <p>Use of Advanced Technology X-Rays Machine for optimum accuracy of Gold</p>
                            <p>There is no Cash transaction only NEFT/RTGS </p>
                        </div>
                        <div class="work-box">
                            <div class="work-head">
                                <h3>
                                    <span>02</span>Why customer's want to sell their old gold to IMAGOLD?</h3>
                            </div>
                            <p>Aims to bring Personalised service in gold selling process.</p>
                            <p>Offers you Competitive rates for you old gold.</p>
                            <p>We purchase any quality above 18k old gold which you intent to sell</p>
                            <p>No Hidden / Processing charges of any type.</p>
                            <p>We do not have any deviations from the process defined.</p>
                            <p>We use best in class X-Ray technology for the evaluation of your gold</p>
                            <p>Easy Documentation &amp; Transparency in complete process.</p>
                            <p>There is no Cash transaction only NEFT/RTGS</p>
                            <p>Nominal Melting &amp; refinary charges</p>
                        </div>
                        <div class="work-box">
                            <div class="work-head">
                                <h3>
                                    <span>03</span>What is the use of purchasing Gold/Silver coins and bars with IMAGOLD?</h3>
                            </div>
                            <p>One of the reputed Gold showroom in India.</p>
                            <p>IMA Jewels is the highest GST payers accross PAN india amongts all jewellers</p>
                            <p>Offers you competitive rates</p>
                            <p>Protects you from the uncertain situations in future</p>
                            <p>Gold coins are asset without any expiry date</p>
                            <p>Available readily in denominations of 1,2,4,5,8,10,20,50,100,500 &amp; 1000 gms</p>
                            <p>Orders of upto 100kg of gold and upto 1000kg of silver can be fulfilled on daily basis.</p>
                            <p>Immediate delivery upon NEFT/RTGS confirmation &amp; transparency in complete process</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-content image_section_center flex">
                <!-- <img src="images/about-banner.jpg" alt="" /> -->
                <video poster="images/about-banner.jpg" id="bgvid" playsinline autoplay muted loop preload="none">
                    <!-- <source src="http://thenewcode.com/assets/videos/polina.webm" type="video/webm"> -->
                    <source src="images/shutterstock_v13949594.mp4" type="video/mp4">
                </video>

                <div class="about-tag">
                    <span class="about-header">IMA Gold.</span>
                    <span class="about-header">Extraordinaire.</span>
                    <div class="about-tag-text">

                        <span>IMA Gold is a leading We providing cost-effective, and timely solutions.</span>

                    </div>
                    <div class="join-team-btn-outline">
                        <a class="btn-1" href="mailto:info@imonetaryadvisory.com">
                            <svg>
                                <rect x="0" y="0" fill="none" width="100%" height="100%"></rect>
                            </svg>Join Our Team
                        </a>
                    </div>
                </div>

            </div>

            <div class="process-table clearfix ">
                <div class="gold-loan animate-content">
                    <h2>Gold Loan Procedure</h2>
                    <ul class="procedure-lists">
                        <li>Purity check and estimation will be given</li>
                        <li>KYC procedures and Confirm from accounts team if the membership with IMA is still active</li>
                        <li>PAN + Aadhar / DL/ Passport/ Voter ID + Bank Document ( Passbook / Copy of Cancelled Cheque ){ copy of each document to be self attested } and Approval from the manager</li>
                        <li>Print the payment reciept post approval handover customer copies of payment reciept and estimation</li>
                        <li>Advice customer that they will receive funds in their account in 24 hours ( Subjected To Bank Holidays )</li>

                    </ul>
                </div>
                <div class="gold-loan animate-content">
                    <h2>Example for Gold Loan Procedures</h2>
                    <ul class="procedure-lists">
                        <li>Customer gets 100 gm Gold ( 916 / 22ct ).</li>
                        <li>We calculate based on 24ct hence we would get 91 gm of 24ct gold.</li>
                        <li>We multiply the same with the current market price of 24ct.</li>
                        <li>Then we divide the total value by 2 and offer the same amount as loan to our customer.</li>
                        <li>We do not charge processing fee or appraisal fee.</li>
                        <li>We have a nominal charge of Rs 12 per gram per month.</li>
                        <li>As per the above mentioned example the monthly charges for 100gm of gold will be Rs 1200 per month.</li>
                    </ul>
                </div>
                <div class="calculate-table animate-content">
                    <h2>Example of calculations</h2>
                    <table class="table table-bordered" id='customers'>
                      
                        <tbody>
                           <tr>
                                <td>Customer Gold Weight</td>
                                <td>100 gm</td>
                            </tr>
                            <tr>
                                <td>Customer Gold Ct</td>
                                <td>22ct</td>
                            </tr>
                            <tr>
                                <td>24 ct weight</td>
                                <td>91 gm</td>
                            </tr>
                            <tr>
                                <td>24ct Price for the day</td>
                                <td>3180 (subject to change)</td>
                            </tr>
                            <tr>
                                <td>Gold Value Estimation</td>
                                <td>(91*3180) = 2,89,380</td>
                            </tr>
                            <tr>
                                <td>Loan Amount that can be offered</td>
                                <td>(2,89,380/2) = 1,44,690</td>
                            </tr>
                            <tr>
                                <td>Value Charges per gram</td>
                                <td>6</td>
                            </tr>
                            <tr>
                                <td>Security Charges per gram</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>Misc Charges per gram</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>Total Charges per gram</td>
                                <td>12</td>
                            </tr>
                            <tr>
                                <td>Total Charges as per the weight</td>
                                <td>(91*12) = 1092 per month</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--2-->
            <div class="full-width">
                <div class="process-table clearfix ">
                    <div class="gold-loan animate-content">
                        <h2>Terms during loan Tenurity</h2>
                        <ul class="procedure-lists">
                            <li>No processing Fee , No Appraiser Fee , No Pre closure charges and No late payment charges.</li>
                            <li>Liberty to plan your repayment as per your convenience in 365 days.</li>
                            <li>Partial payments are allowed you can make the payments as per your convenience </li>
                            <li>If you make a payment and you have pledged multiple ornaments we could release the ornaments worth the amount paid and with immediate effect custody charges will be for the remaining ornaments in our vault</li>
                            <li>If you make a payment and you have pledged single ornaments we wouldn’t release the ornament hence the Custody charges will remain the same however payment will be still considered towards the principle amount.</li>
                            <li>Post 365 days we would provide additional 30days for the borrower to revert.</li>
                            <li>If the Borrower does not get back we would melt the gold and have it sold.</li>
                            <li>The difference amount after we take our loan amount + Custody charges + 3% melting charges, we will transfer to the Borrowers Bank Account.</li>
                        </ul>
                    </div>
                    <div class="calculate-table animate-content">
                        <h2>Example of calculations</h2>
                        <table class="table table-bordered" id='customers'>
                          
                            <tbody>
                               <tr>
                                    <td>Loan Amount</td>
                                    <td>1,44,690</td>
                                </tr>
                                <tr>
                                    <td>Custody Charges per month</td>
                                    <td>1,092</td>
                                </tr>
                                <tr>
                                    <td>Custody Charges for 13 months</td>
                                    <td>14,196</td>
                                </tr>
                                <tr>
                                    <td>3% Melting Charges</td>
                                    <td>9,009</td>
                                </tr>
                                <tr>
                                    <td>Assumed price per gram after 13 months</td>
                                    <td>3,300</td>
                                </tr>
                                <tr>
                                    <td>Weight of 24ct after melting</td>
                                    <td>91 gm</td>
                                </tr>
                                <tr>
                                    <td>Assumed Value of melted gold</td>
                                    <td>3,00,300</td>
                                </tr>
                                <tr>
                                    <td>Total Deductions (MC, Custody, Loan amount)</td>
                                    <td>1,67,895</td>
                                </tr>
                                <tr>
                                    <td>Amount Transferred to borrowers account</td>
                                    <td>1,32,405</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                     <div class="gold-loan animate-content">
                        <h2>Loan Renewal or Extension</h2>
                        <ul class="procedure-lists">
                            <li>If Borrowers Requires more time they have an option to make the payment of Custody charges and we will Re evaluate and Renew the loan for another 365 days.</li>
                           
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="process-table clearfix ">
                
                <div class="gold-loan animate-content">
                    <h2>Example for OLD Gold Purchase Procedures</h2>
                    <ul class="procedure-lists">
                        <li>Customer gets  100 gm  Gold ( 916 / 22ct ).</li>
                        <li>We calculate based on  24ct hence we would get  91 gm of 24ct gold.</li>
                        <li>We multiply the same with the current market price of 24ct.</li>
                        <li>We do not charge processing fee or appraisal fee.</li>
                        <li>There will be a depreciation of 3% over all</li>
                    </ul>
                </div>
                <div class="calculate-table animate-content">
                    <h2>Example of calculations</h2>
                    <table class="table table-bordered" id='customers'>
                      
                        <tbody>
                            <tr>
                                <td>Customer Gold Weight</td>
                                <td>100 gm</td>
                            </tr>
                            <tr>
                                <td>Customer Gold Ct</td>
                                <td>22ct</td>
                            </tr>
                            <tr>
                                <td>24 ct weight</td>
                                <td>91 gm</td>
                            </tr>
                            <tr>
                                <td>24ct Price for the day</td>
                                <td>3180 (subject to change)</td>
                            </tr>
                            <tr>
                                <td>Gold Value Estimation</td>
                                <td>(91*3180) = 2,89,380</td>
                            </tr>
                            <tr>
                                <td>Melting Charges/td>
                                <td>57.876</td>
                            </tr>
                            <tr>
                                <td>Refinery Charges</td>
                                <td>2893.8</td>
                            </tr>
                            <tr>
                                <td>Final Estimation</td>
                                <td>2,86,428.324</td>
                            </tr>
                         
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- process section start -->
            <!--<div class="process_section work-mobile">
                <div class="">
                    <div class="section-heading center work-process-heading">Work process
                    </div>
                    <div class="row">
                        <div class="process-text-wrapper text-center">
                            <ol class="process_text text--medium js-text-list">
                                <li class="is_active" id="step0">
                                    <p data-plugin="appear" data-appear-effects="text">
                                        We trust our work process that makes us to achieve outstanding results. We create strategies based on ground reality and then build <br>
                                        out amazing ideas.</p>
                                </li>
                                <li id="step1">
                                    <p>We ask clients to fill creative brief about there requirements. We do multiple discussions & brief meetings to understand project. And then, we will come up with quote & timelines.</p>

                                </li>
                                <li class="" id="step2">
                                    <p>Once the quotes is approved, then we start on brainstorming and create wireframes to share our ideas & directions. After approval on concept wire-framing, we start on real designs.</p>
                                </li>
                                <li class="" id="step3">
                                    <p>After designs submission, we ask client to review and give constructive feedbacks. Nothing is perfect and by collaboration we achieve the best design with client 100% satisfaction.</p>
                                </li>
                                <li class="" id="step4">
                                    <p>To achieve best functional designs, we prepare full wrap-up with detailed documentation for developers. And process all necessary assets with managed source files.

                                    </p>
                                </li>
                                <li class="" id="step5">
                                    <p>After final round of changes, We again check full design to make sure everything is on design standards. Like grids, use of fonts, spacings, colors & buttons hierarchy.</p>

                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="process_list">
                        <ol class="row js-link-list">
                            <li class="col" data-href="#step1">
                                <div class="process_list_item">
                                    <span>Documents Required</span>
                                </div>
                            </li>
                            <li class="col" data-href="#step2">
                                <div class="process_list_item">
                                    <span>Gold Loan Procedure</span>
                                </div>
                            </li>
                            <li class="col" data-href="#step3">
                                <div class="process_list_item">
                                    <span>Loan Renewal or Extension</span>
                                </div>
                            </li>
                            <li class="col" data-href="#step5">
                                <div class="process_list_item">
                                    <span>Final Touches</span>
                                </div>
                            </li>
                            <li class="col" data-href="#step4">
                                <div class="process_list_item">
                                    <span>Wrap-Up & Handover</span>
                                </div>
                            </li>
                        </ol>
                    </div>
                </div>

            </div>-->
            <!-- process section end -->
        </div>
        <!-- <div class="team-info  animate-content">
            <div class="team-content">
                <h2>Our people
                </h2>
                <p class="serif">We love our jobs, we love our customers, and we love providing absolutely outstanding customer service. And we travel alot to stay inspired & energetic.
                </p>
            </div>
            <div class="team-images">
                <div class="small-images">
                    <div class="img-row">
                        <img src="images/people/img-1.png" alt="" class="animate-content transition-duration-1">
                        <img src="images/people/img-2.png" alt="" class="animate-content transition-duration-2">
                    </div>
                    <div class="img-row">
                        <img src="images/people/img-3.png" alt="" class="animate-content transition-duration-1">
                        <img src="images/people/img-4.png" alt="" class="animate-content transition-duration-2">
                        <img src="images/people/img-5.png" alt="" class="animate-content transition-duration-3">
                    </div>
                </div>
                <div class="people-main-image">
                    <img src="images/people/main-img.png" alt="">
                </div>
            </div>
        </div>-->
        <!-- office_section start -->
        <div class="office_section">
            <div class="team-content office animate-content">
                <h2>Our Office
                </h2>
                <p class="serif">We believe, to do outstanding work we need amazing place to collaborate. So we share luxury office space for team and premium PMS Basecamp 3 for Clients as our virtual office.
                </p>
            </div>
            <div class="row">
                <div class="column ">
                    <img src="images/office/1.JPG" class="animate-content transition-duration-1">
                    <img src="images/office/4.JPG" class="animate-content">

                </div>
                <div class="column ">
                    <img src="images/office/2.JPG" class="animate-content transition-duration-2">
                    <img src="images/office/5.JPG" class="animate-content">

                </div>
                <div class="column ">
                    <img src="images/office/3.JPG" class="animate-content transition-duration-3">

                </div>

            </div>
        </div>
        <!-- office_section end -->

        <div class="honors max-container  animate-content">
            <h2>Honors &amp; Awards
            </h2>
            <p>It's true. We think we're pretty awesome. But it's not just us. We have proof. And honestly, we couldn't be prouder,
                or more humble. </p>
            <div class="flex flex-justify-between flex-align-start honors-grid">
                <div class="honor-box flex flex-column">
                    <!--<p class="image">
                        <img src="images/design.jpg" alt="">
                    </p>-->
                    <!-- <h2>2</h2>-->
                    <h4>why customer would avail a gold loan with IMAGOLD?</h4>
                    <ul>
                        <li>Interest free</li>
                        <li>No Processing fee </li>
                        <li>No Appraisers fee</li>
                        <li>No preclosure Charges</li>
                        <li>No Late Payment charges</li>
                        <li>Nominal Security charges for the safe custody </li>
                        <li>Easy Documentation</li>
                        <li>Transparency in the complete process</li>
                        <li>Use of Advanced Technology X-Rays Machine for optimum accuracy of Gold</li>
                        <li>There is no Cash transaction only NEFT/RTGS </li>
                    </ul>
                </div>
                <div class="honor-box flex flex-column">
                    <!--<p class="image">
                        <img src="images/design.jpg" alt="">
                    </p>-->
                    <!-- <h2>14</h2>-->
                    <h4>Why customer's want to sell their old gold to IMAGOLD?</h4>
                    <ul>
                        <li>Aims to bring Personalised service in gold selling process.</li>
                        <li>Offers you Competitive rates for you old gold.</li>
                        <li>We purchase any quality above 18k old gold which you intent to sell</li>
                        <li>No Hidden / Processing charges of any type.</li>
                        <li>We do not have any deviations from the process defined.</li>
                        <li>We use best in class X-Ray technology for the evaluation of your gold</li>
                        <li>Easy Documentation</li>
                        <li>Transparency in the complete process</li>
                        <li>There is no Cash transaction only NEFT/RTGS </li>
                        <li>Nominal Melting and refinary charges</li>
                    </ul>
                </div>
                <div class="honor-box flex flex-column">
                    <!-- <p class="image">
                        <img src="images/awwwards.jpg" alt="">
                    </p>-->
                    <!--<h2>2</h2>-->
                    <h4>What is the use of purchasing Gold/Silver coins and bars with IMAGOLD?</h4>
                    <ul>
                        <li>One of the reputed Gold showroom in India.</li>
                        <li>IMA Jewels is the highest GST payers accross PAN india amongts all jewellers </li>
                        <li>No Appraisers fee</li>
                        <li>No preclosure Charges</li>
                        <li>No Late Payment charges</li>
                        <li>Nominal Security charges for the safe custody </li>
                        <li>Easy Documentation</li>
                        <li>Transparency in the complete process</li>
                        <li>Use of Advanced Technology X-Rays Machine for optimum accuracy of Gold</li>
                        <li>There is no Cash transaction only NEFT/RTGS </li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- clients section start -->
        <div class="section-heading center">Happy Clients
        </div>
        <div class="clients_section_outer">
            <div class="clients_section max-container  animate-content">

                <div class="slider-outer">
                    <div class="testimonial-slider">
                        <div class="testimonial">
                            <div class="testimonial-inner">
                                <div class="testimonial-text">
                                    I've never had a challenge finding designers who are talented. I have, however, found it challenging to find someone who
                                    has both a great attitude and who I can trust fully. Not only is Surinder a very talented
                                    designer, but I trust him to do what he says he will do when he says he'll do it. He's
                                    an absolute joy to work with.
                                </div>
                                <div class="testimonial-client-info">
                                    <div class="client-name">AVIN KLINE </div>
                                    <div>COFOUNDER & CEO , Web Success Agency</div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-inner">
                                <div class="testimonial-text">
                                    Surinder and the team at IMA Gold do truly amazing work. Throughout the almost two years that I have worked with them, on everything
                                    from basic business cards, to drip emails, to a custom website built from scratch, and
                                    everything in between, they have truly helped me build an inspiring, stylistic, attention-getting
                                    identity for our company. You won’t be disappointed.
                                </div>
                                <div class="testimonial-client-info">
                                    <div class="client-name">SHEILA RAPER </div>
                                    <div>National Marketing Director, Estates Company USA
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-inner">
                                <div class="testimonial-text">
                                    Team IMA Gold, thanks for your outstanding, creative and on time delivery of awesome Screen designs and Info-graphics. It is
                                    really hard to find reliable partners these days but with IMA Gold we found a long-term support-partner.
                                </div>
                                <div class="testimonial-client-info">
                                    <div class="client-name">ULI S.</div>
                                    <div>CEO of German Design Agency
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-inner">
                                <div class="testimonial-text">
                                    I discovered IMA Gold while searching for templates on the net. Their design examples impressed me and I decided to make contact.
                                    From start to end they provided fast response and fast delivery and they understod my
                                    needs and provided a design that we can't wait to implement and get online. We have already
                                    engaged with IMA Gold for more work.
                                </div>
                                <div class="testimonial-client-info">
                                    <div class="client-name">Torben Sørensen </div>
                                    <div>CEO of Casalogic
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-inner">
                                <div class="testimonial-text">
                                    Surinder had an immediate understanding of our needs, and with his guiding professional advice, completed our project on
                                    time and on budget. We were more than happy with Surinder's professional service.
                                </div>
                                <div class="testimonial-client-info">
                                    <div class="client-name">Christine &amp; Heck </div>
                                    <div>CEO of Greenskin Media</div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-inner">
                                <div class="testimonial-text">
                                    I have worked with countless designers over the past decade and I always struggled to find the right person. Surinder has
                                    always provided me with professional and clean looking designs that definitely stand
                                    out and demand attention. If you want the job done right the first time Surinder is the
                                    guy are looking for!
                                </div>
                                <div class="testimonial-client-info">
                                    <div class="client-name">Parveen Thakur</div>
                                    <div>CEO of Envyus Media Pvt Ltd</div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-inner">
                                <div class="testimonial-text">
                                    We have used IMA Gold for improvements and a redesign of our current website. This was a challanging job, because it involved
                                    coding in a custom made website and not a template, but IMA Gold met all our expectations
                                    and provided a stunning result in record time. We will definitley use IMA Gold again for
                                    future implications ect. and have already talked with them about new ideas and wishes
                                    for our website.
                                </div>
                                <div class="testimonial-client-info">
                                    <div class="client-name">Christian Mygh </div>
                                    <div>CEO of REC Watches</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- clients section end -->


        <?php include 'footer.php';?>
    </div>


    <script src="js/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.isOnScreen.js"></script>
    <script>
        jQuery(document).ready(function($) {
            jQuery(window).scroll(function() {
                if (jQuery('.eb-image').isOnScreen()) {

                    jQuery('svg circle').addClass('animate-circle');

                } else {
                    jQuery('svg circle').removeClass('animate-circle');

                }
            })



            jQuery('.testimonial-slider').slick({
                dots: true,
                arrows: false,
                speed: 1000,
                autoplay: true,
                autoplaySpeed: 1000,
                cssEase: 'ease-in-out'
            });



            jQuery('.slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                focusOnSelect: true
            });

            jQuery(".scroll_down").click(function(event) {
                event.preventDefault();
                var clink = jQuery(this).attr('href');
                if (jQuery(window).width() > 767) {
                    jQuery('html,body').animate({
                        scrollTop: jQuery(clink).offset().top - 25
                    }, 'slow');
                } else {
                    $('#page').animate({
                        scrollTop: $(clink).parents('#page').scrollTop() + $(clink).offset().top - $(clink).parents('#page').offset().top - 40
                    }, 'slow');
                }
            });


            jQuery('.work-grid').slick({
                slidesToShow: 1,
                mobileFirst: true,
                slidesToScroll: 1,
                arrows: false,
                dots: true,
                responsive: [{
                    breakpoint: 767,
                    settings: 'unslick'
                }]
            });

            jQuery('.honors-grid').slick({
                slidesToShow: 1,
                mobileFirst: true,
                slidesToScroll: 1,
                arrows: false,
                dots: true,
                responsive: [{
                    breakpoint: 767,
                    settings: 'unslick'
                }]
            });
        });

    </script>
    <script>
        jQuery(document).ready(function($) {
            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;
            jQuery('body, html, #page').height(h);

            jQuery('.siteheader-toggle').click(function(event) {
                jQuery('.siteheader-menu').toggleClass('open_menu');
                if (jQuery('.siteheader-menu').hasClass('open_menu') == true) {
                    jQuery('.siteheader-menu').height(((h * 88) / 100).toFixed(0));
                } else {
                    jQuery('.siteheader-menu').height(65);
                }
            });

        });

        jQuery(window).resize(function($) {
            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;
            jQuery('body, html, #page').height(h);
        });

        $(document).on('mouseenter', '.process_list ol li', function() {
            $('.process_list ol li').removeClass('is_active');
            $(this).addClass("is_active");
            $itstext = $(this).data('href');
            $('.process_text li').removeClass('is_active');
            $($itstext).addClass('is_active');
            console.log($itstext);
        });
        $(document).on('mouseleave', '.process_list ol', function() {
            $('.process_text li:first-child').addClass('is_active');
            $('.process_text li').not($('.process_text li:first-child')).removeClass('is_active');
        })

    </script>
    <script src="js/scripts.js"></script>

</body>

</html>
