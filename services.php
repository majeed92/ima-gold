<!doctype html>
<html lang="en" class="no-js is-not-ie-edge has-native-scroll">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <title>Services | IMA Gold</title>
    <meta name="keywords" content="">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Roboto+Slab:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="fonts/style.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/blue-theme.css">
    <style>
        #player .ytp-pause-overlay {
            display: none !important;

    </style>
    <script>
        document.documentElement.className = document.documentElement.className.replace('no-js', 'js')

    </script>
    <script src="js/jquery.min.js"></script>

</head>

<body>

    <div class="loader">
        <div class="loading"></div>
    </div>

    <div id="page">
        <?php include 'header.php';?>

        <div class="about_content service_content">
            <div class="inside_content banner_content">
                <h1 class="page_title">
                    <span>Learn More </span>About Our Services</h1>
                <p>Listed below are some of our Services.</p>
            </div>
        </div>

        <!--   <div class="service-content  animate-content">
        </div>-->


        <div class="service-accordion">

            <div class="panel-group" id="accordion">
                <!--<h2 class="page_titles">Our Services</h2>-->
                <div class="panel panel-default animate open animate-content">
                    <div class="panel-heading" id="goldloan">
                        <h4 class="panel-title">
                            <a href="#goldloan">
                                <span class="number">01</span> Gold Loan Procedure
                            </a>
                        </h4>
                    </div>
                    <div id="goldloan" class="panel-collapse collapse in">
                        <div class="panel-body flex flex-justify-beetween">
                            <!--<div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <!-- <p>WE ACCEPT OLD GOLD FROM 18 CARAT TO 24 CARAT</p>-->

                                <ul>
                                    <li>Purity check and estimation will be given</li>
                                    <li>KYC procedures</li>
                                    <li>Confirm from accounts team if the membership with IMA is still active</li>
                                    <li>PAN + Aadhar / DL / Passport / Voter ID + Bank Document ( Passbook / Copy of Cancelled Cheque ) { copy of each document to be self attested}</li>
                                    <li>Approval from the manager</li>
                                    <li>Print the payment reciept post approval handover customer copies of payment reciept and estimation</li>
                                    <li>Advice customer that they will receive funds in their account in 24 hours ( Subjected To Bank Holidays )</li>
                                </ul>
                                <!-- <a href="#" class="btn btn-link">KNOW MORE</a> -->

                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="loan">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#loan">
                                <span class="number">02</span> Terms during loan Tenurity
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-3">
                                <img src="images/office/1.jpg" alt="">
                            </div>

                            <div class="col-9">

                                <ul>
                                    <li>No processing Fee.</li>
                                    <li>No Appraiser Fee.</li>
                                    <li>No Pre closure charges.</li>
                                    <li>No late payment charges.</li>
                                    <li>Liberty to plan your repayment as per your convenience in 365 days.</li>
                                    <li>Partial payments are allowed you can make the payments as per your convenience </li>
                                    <li>If you make a payment and you have pledged multiple ornaments we could release the ornaments worth the amount paid and with immediate effect custody charges will be for the remaining ornaments in our vault</li>
                                    <li>If you make a payment and you have pledged single ornaments we wouldn’t release the ornament hence the Custody charges will remain the same however payment will be still considered towards the principle amount.</li>
                                    <li>Post 365 days we would provide additional 30days for the Borrower to revert.</li>
                                    <li>If the Borrower does not get back we would melt the gold and have it sold.</li>

                                </ul>
                                <p>The difference amount after we take our loan amount + Custody charges + 3% melting charges, we will transfer to the Borrowers Bank Account.</p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="oldgold">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#oldgold">
                                <span class="number">03</span> Old Gold Purchase Procedure

                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!--  <div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <ul>
                                    <li>Purity check and estimation will be given</li>
                                    <li>No processing Fee.</li>
                                    <li>No Appraiser Fee.</li>
                                    <li>KYC procedures</li>
                                    <li>PAN + Aadhar / DL / Passport / Voter ID + Bank Document ( Passbook / Copy of Cancelled Cheque ) { copy of each document to be self attested}</li>
                                    <li>Approval from the manager</li>
                                    <li>Print the payment reciept post approval handover customer copies of payment reciept and estimation</li>
                                    <li>Advice customer that they will receive funds in their account in 24 hours ( Subjected To Bank Holidays ) </li>
                                </ul>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="carryme">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#carryme">
                                <span class="number">04</span> What Should I Carry With Me?
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!--   <div class="col-3">
                                <p class="lead">Answer</p>
                            </div>
-->
                            <div class="col-9">
                                <p>KYC MANDATORY ( PAN + Aadhar/ DL/ Voter ID / Passport + Bank Passbook/ Cancelled Cheque Copy)</p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="coinsbars">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#coinsbars">
                                <span class="number">05</span> Coins And Bars Purchase
                            </a>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!-- <div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <ul>
                                    <li>Now at IMA you can avail pure 24K Gold and silver Coins and Bars at the best price.</li>
                                    <li>Gold coins are asset without any expiry date Available readily in denominations of 1,2,4,5,8,10,20,50,100,500 &amp; 1000 gms.</li>
                                    <li>Orders of upto 100kg of gold and upto 1000kg of silver can be fulfilled on daily basis.</li>
                                    <li>Immediate delivery upon NEFT/RTGS confirmation transparency in complete process.</li>
                                    <li>Pan Number Manadatory on purchase value above 2 lakh</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include 'footer.php';?>

    </div>
    <script src="https://www.youtube.com/iframe_api"></script>
    <script>
        jQuery(document).ready(function($) {

            jQuery('#accordion .panel-title a:not(.oldgold)').click(function(event) {
                event.preventDefault();
                var id = jQuery(this).attr('href');

                jQuery('#accordion .panel').removeClass('open');
                jQuery('#accordion .panel-collapse').slideUp();
                jQuery(this).parents('.panel').addClass('open').find('.panel-collapse').slideDown();

                var clink = jQuery(this);

                window.setTimeout(function() {
                    if (jQuery(window).width() > 767) {
                        jQuery('html,body').animate({
                            scrollTop: jQuery(clink).offset().top - 25
                        }, 'slow');
                    } else {
                        $('#page').animate({
                            scrollTop: $(clink).parents('#page').scrollTop() + $(clink).offset().top - $(clink).parents('#page').offset().top - 40
                        }, 'slow');
                    }
                }, 450);


            });




        });

    </script>
    <!-- open service from url start -->
    <script>
        jQuery(document).ready(function($) {
            if (window.location.href.indexOf('#') !== -1) {
                var service_id = window.location.href.substring(window.location.href.indexOf('#'));
                console.log(service_id)
                var _this = $("a[href='" + service_id + "']");
                $(_this).trigger('click');
            }
        });
        // Services option click from menu
        function menuClick(e, link) {
            e.preventDefault();
            $('#nav-toggle').removeClass("active");
            $("#menu-navbar").removeClass("open");
            var _this = $("a[href='#" + link + "']");
            $(_this).trigger('click');
            setTimeout(function() {
                windowscrollPosition = $(window).scrollTop();
                $('html,body').animate({
                    scrollTop: windowscrollPosition - 50
                }, 'slow');
            }, 1000)
        }

    </script>
    <!-- open service from url end -->

    <script src="js/scripts.js"></script>
    <script>
        jQuery(document).ready(function($) {
            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;
            jQuery('body, html, #page').height(h);

            jQuery('.siteheader-toggle').click(function(event) {
                jQuery('.siteheader-menu').toggleClass('open_menu');
                if (jQuery('.siteheader-menu').hasClass('open_menu') == true) {
                    jQuery('.siteheader-menu').height(((h * 88) / 100).toFixed(0));
                } else {
                    jQuery('.siteheader-menu').height(65);
                }
            });

        });

        jQuery(window).resize(function($) {
            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;
            jQuery('body, html, #page').height(h);
        });

    </script>


</body>

</html>
