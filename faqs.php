<!doctype html>
<html lang="en" class="no-js is-not-ie-edge has-native-scroll">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <title>Faqs | IMA Gold</title>
    <meta name="keywords" content="">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i|Roboto+Slab:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="fonts/style.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/blue-theme.css">

    <script>
        document.documentElement.className = document.documentElement.className.replace('no-js', 'js')

    </script>
    <script src="js/jquery.min.js"></script>


</head>

<body>

    <div class="loader">
        <div class="loading"></div>
    </div>

    <div id="page">
        <?php include 'header.php';?>

        <div class="about_content">
            <div class="inside_content banner_content">
                <h1 class="page_title">
                    <span>IMA Gold</span> FAQ'S</h1>
                <p>We create just about everything. Listed below are some of our specialties.</p>
            </div>
        </div>

        <!--   <div class="service-content  animate-content">
        </div>-->


        <div class="service-accordion">

            <div class="panel-group" id="accordion">
                <h2 class="page_titles">OLD GOLD PURCHASE</h2>
                <div class="panel panel-default animate open animate-content">
                    <div class="panel-heading" id="headingOne">
                        <h4 class="panel-title">
                            <a href="#goldloan">
                                <span class="number">01</span> What type of Gold do we accept?
                            </a>
                        </h4>
                    </div>
                    <div id="goldloan" class="panel-collapse collapse in">
                        <div class="panel-body flex flex-justify-beetween">
                            <!--<div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <p>We accept old gold from 18 carat to 24 carat</p>
                                <!--
                                <ul>
                                    <li>Logo</li>
                                    <li>Business Card</li>
                                    <li>Letterhead</li>
                                    <li>Stationary</li>
                                    <li>Presentations Stuff</li>
                                    <li>Email Signature</li>
                                    <li>Email Creatives</li>
                                </ul>-->
                                <!-- <a href="#" class="btn btn-link">KNOW MORE</a> -->

                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="loan">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#loan">
                                <span class="number">02</span> How will you value my gold?
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!--  <div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <p>3% Depreciation as melting charges (Applicable on 24 carat as well)</p>
                                <ul>
                                    <li>The gold will be valued as 24 carat only and would be priced on the prevailing rate of 24 carat</li>
                                </ul>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#mobile_apps">
                                <span class="number">03</span> Do i get cash payment?

                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!--  <div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <p>Funds will be transferred on the next working day only to bank account as we completely avoid cash transactions</p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="oldgold">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#oldgold">
                                <span class="number">04</span> What sholud i carry with me?

                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!--   <div class="col-3">
                                <p class="lead">Answer</p>
                            </div>
-->
                            <div class="col-9">
                                <p>KYC mandatory ( PAN + Aadhar/ DL/ Voter ID / Passport + Bank Passbook/ Cancelled cheque copy)</p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="carryme">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#carryme">
                                <span class="number">05</span> Why should I select IMA Gold ver others?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!-- <div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <p>We at IMA , offer you the best value for your gold as per the prevailing market value with best in class technology to evaluate your gold purity and give you complete transparency in the process by experienced appraisers.</p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="coinsbars">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#coinsbars">
                                <span class="number">06</span> Can i sell gold which i bought from other country?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!--<div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <p>We accept gold from 18 carat to 22 carat respective of where gold was purchased from!</p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="advertisements">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#advertisements">
                                <span class="number">07</span> My ornaments are from IMA itself will you still depreciate 3% as melting charges?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse7" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!--<div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <p>Yes we do depreciate as we melt all the ornaments irrespective of where it was purchased from.</p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">08</span> I have my relatives KYC documents but he is out of town and I don’t have any of my documents so can I sell my gold ?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <!--<div class="col-3">
                                <p class="lead">Answer</p>
                            </div>-->

                            <div class="col-9">
                                <p>No , it is mandatory that the person whose documents are provided should be physically present for our KYC procedures.</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">09</span> Can I sell my silver / platinum / diamond jewelry at IMA Gold?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>No we only purchase gold we do not purchase, silver / platinum / diamonds</p>

                            </div>

                        </div>
                    </div>
                </div>
                <!--sdf-->
                <h2 class="page_titles"> GOLD LOAN</h2>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">10</span> Should IMA Membership be active?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>IMA Membership has to be active!!!</p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">11</span> Can i pledge gold which i bought from other country?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>We accept gold from 18 carat to 22 carat only Respective of where gold was purchased from!</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">12</span> My Membership with IMA is active and I have submitted my KYC already, should I still provide my KYC?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>Since we are a different entity altogether you will still have to provide your KYC to us. </p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">13</span> How much loan can i get?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">

                                <p>The gold should ne minimum of 10 grams to 4 lakh worth of gold of which we would give 50$ as loan amount that is 2 lakhs, which is the maximum loan amount that a member could avail from a membership</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">14</span> Can any of my family member's come and release the gold I've pledged?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>No third party will be entertained on transactions only the borrower has to come to release ornaments</p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">15</span> What is the duration of Loan?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>Loan duration is 365 days!</p>
                                <ul>
                                    <li>After 365 days the borrower can pay custody charges and renew there loan duration</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">16</span> I have my relatives KYC documents but he is out of town and I don’t have any of my documents ?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>No , it is mandatory that the person whose documents are provided should be physically present for our KYC procedures.</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">17</span> Do i get cash payment?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>Funds will be transferred on the next working day only to bank accoint as we completely avoid cash transactions</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">18</span> What should i carry with me?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>KYC Mandatory:-Client ID ( PAN + Aadhar/ DL/ Voter ID / Passport + Bank Passbook/ Cancelled Cheque Copy)</p>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">19</span> Can I pay my custody charges or my loan amount through cash or it is mandate to make online payment?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>Yes, You can make cash payments. We do not have any obligations on the repayment mode.</p>

                            </div>

                        </div>
                    </div>
                </div>
                <h2 class="page_titles">IMA GOLD, COINS &amp; BARS </h2>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">20</span> What are the denominations available in gold and silver?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <!-- <p><b>DENOMINATIONS OF GOLD COINS :</b> 1 GRAMS, 2 GRAMS, 4GRAMS, 8GRAMS </p>
                                <p><b>DENOMINATION OF GOLD BARS : </b> 5 GRAMS, 10 GRAMS, 20 GRAMS</p>
                                <p><b>SILVER :</b> 5/10/100/500/30KG </p>-->

                                <p><b>Denominations of gold coins :</b> 1Grams, 2Grams, 4Grams, 8Grams</p>
                                <p><b>Denomination of gold bars :</b> 5Grams, 10Grams, 20Grams</p>
                                <p><b>Silver :</b> 5/10/100/500/30Kg</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">21</span> Why should I choose IMA Gold over others in market to buy gold?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>At IMA you can avail pure 24K Gold and silver Coins and Bars at the best price compared to the market at the prevailing market value.gold is always one of the best assets that you can have and stands out on to your portfolio.</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">22</span> What is the rate of gold?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>The rate is prevaling as per JAB ( Jewelers Associasion Bangalore) + Rs 20/- for Dye charge + 3%GST</p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default animate animate-content">
                    <div class="panel-heading" id="dev_services">
                        <h4 class="panel-title">
                            <a class="collapsed" href="#dev_services">
                                <span class="number">23</span> What is the rate of silver?
                            </a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body flex flex-justify-beetween">

                            <div class="col-9">
                                <p>The rate is prevaling as per JAB( Jewelers Associasion Bangalore) + Rs 10/- For Dye Charge + 3%GST</p>

                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>

        <?php include 'footer.php';?>

    </div>

    <script>
        jQuery(document).ready(function($) {

            jQuery('#accordion .panel-title a:not(.oldgold)').click(function(event) {
                event.preventDefault();
                var id = jQuery(this).attr('href');

                jQuery('#accordion .panel').removeClass('open');
                jQuery('#accordion .panel-collapse').slideUp();
                jQuery(this).parents('.panel').addClass('open').find('.panel-collapse').slideDown();

                var clink = jQuery(this);

                window.setTimeout(function() {
                    if (jQuery(window).width() > 767) {
                        jQuery('html,body').animate({
                            scrollTop: jQuery(clink).offset().top - 25
                        }, 'slow');
                    } else {
                        $('#page').animate({
                            scrollTop: $(clink).parents('#page').scrollTop() + $(clink).offset().top - $(clink).parents('#page').offset().top - 40
                        }, 'slow');
                    }
                }, 450);


            });

        });

    </script>
    <!-- open service from url start -->
    <script>
        jQuery(document).ready(function($) {
            if (window.location.href.indexOf('#') !== -1) {
                var service_id = window.location.href.substring(window.location.href.indexOf('#'));
                console.log(service_id)
                var _this = $("a[href='" + service_id + "']");
                $(_this).trigger('click');
            }
        });
        // Services option click from menu
        function menuClick(e, link) {
            e.preventDefault();
            $('#nav-toggle').removeClass("active");
            $("#menu-navbar").removeClass("open");
            var _this = $("a[href='#" + link + "']");
            $(_this).trigger('click');
            setTimeout(function() {
                windowscrollPosition = $(window).scrollTop();
                $('html,body').animate({
                    scrollTop: windowscrollPosition - 50
                }, 'slow');
            }, 1000)
        }

    </script>
    <!-- open service from url end -->

    <script src="js/scripts.js"></script>
    <script>
        jQuery(document).ready(function($) {
            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;
            jQuery('body, html, #page').height(h);

            jQuery('.siteheader-toggle').click(function(event) {
                jQuery('.siteheader-menu').toggleClass('open_menu');
                if (jQuery('.siteheader-menu').hasClass('open_menu') == true) {
                    jQuery('.siteheader-menu').height(((h * 88) / 100).toFixed(0));
                } else {
                    jQuery('.siteheader-menu').height(65);
                }
            });

        });

        jQuery(window).resize(function($) {
            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;
            jQuery('body, html, #page').height(h);
        });

    </script>


</body>

</html>
