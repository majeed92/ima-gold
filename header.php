<div class="screen-overlay"></div>
<div class="main-loader" id="loader">
    <div class="icon">
        <div class="outer-layer">
            <div id="B1XatYT3G">
                <div id="rJxXatK6hf_rJOt_14pz" class="image-load">
                    <div id="rJxXatK6hf_SkoFu1Epf">
                        <div id="rJxXatK6hf">
                            <div id="SybXptYTnG">
                                <div id="HyGQ6YF6nf">
                                    <div id="HkXXaFKT3G">
                                        <div id="SkEQaYFanf">
                                            <div>
                                                <img src="images/loader_icons/mob.svg" alt="bg"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="rkOXaYYa3M_rJf9u1Eaf" class="image-load">
                    <div id="rkOXaYYa3M_BJVcuJNTf">
                        <div id="rkOXaYYa3M">
                            <div id="rkYX6ttanz">
                                <div id="ry976tYTnz">
                                    <div id="SJoXpKYThG">
                                        <div id="B13Q6FK6nz">
                                            <div>
                                                <img src="images/loader_icons/tab.svg" alt="bg">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="rkXxQTYYTnG" class="dot">
                    <div id="ry4x7TKKTnz">
                        <div>
                            <img src="images/loader_icons/dot.png" alt="bg"> </div>
                    </div>
                </div>
                <div id="SkLgXTFYahz_r1RiuJVaG" class="image-load">
                    <div id="SkLgXTFYahz_HJRA_JEpG">
                        <div id="SkLgXTFYahz_Hyy3OyEaM">
                            <div id="SkLgXTFYahz">
                                <div id="HywlXatK6hf">
                                    <div id="SkugmptYpnz">
                                        <div id="Bytx76tK62f">
                                            <div>
                                                <img src="images/loader_icons/desktop.svg" alt="bg">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="top-header flex flex-justify-between flex-align-center">
    <a class="logo" href="index.php">
        <img src="images/logo.png" alt="logo" /> </a>
    <div class="navigation_main">
        <button id="nav-toggle" class="navbar-toggle">
            <span class="icon-bar"></span>
        </button>
        <nav id="menu-navbar" class=" flex flex-column">
            <ul class="nav navbar-nav flex flex-align-stretch">
                <li>
                    <a href="index.php">About us
                        <span>About us</span>
                        <span class="small">Design agency. Extraordinaire.</span>
                    </a>
                    <!--<ul class="sub-menu">
                        <li>
                            <a href="about.php">About</a>
                        </li>

                    </ul>-->
                </li>
                <li>
                    <a href="services.php">What we do
                        <span>Services</span>
                        <span class="small">Pure passion. Stunning success.</span>
                    </a>

                    <ul class="sub-menu">
                        <li>
                            <a href="services.php">All Services</a>
                        </li>
                        <li>
                            <a href="services.php#goldloan">Gold Loan Procedure</a>
                        </li>
                        <li>
                            <a href="services.php#loan">Terms during loan Tenurity</a>
                        </li>
                        <li>
                            <a href="services.php#oldgold">Old Gold Purchase Procedure</a>
                        </li>
                        <li>
                            <a href="services.php#carryme">What Should I Carry With Me?</a>
                        </li>
                        <li>
                            <a href="services.php#coinsbars">Coins And Bars Purchase</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="faqs.php">What we love
                        <span>FAQ's</span>
                        <span class="small">Beauty. Brains. Brawn.</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="faqs.php">FAQ'S</a></li>
                    </ul>
                </li>

                <li>
                    <a href="contact.php">Let’s begin!
                        <span>Contact us</span>
                        <span class="small">Your journey starts here.</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="contact.php">Quick Contact</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</header>
