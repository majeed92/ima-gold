<!doctype html>
<html class="no-js">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <title>Gallery | IMA Gold</title>
    <meta name="keywords" content="">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/style.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/new-styles.css">
    <link rel="stylesheet" href="css/blue-theme.css">

    <script>
        document.documentElement.className = document.documentElement.className.replace('no-js', 'js')

    </script>

    <link rel="stylesheet" href="css/modaal.css">

</head>

<body>
    <div class="loader">
        <div class="loading"></div>
    </div>

    <main>
        <?php include 'header.php';?>

        <div>
            <div class="pages js-pages" data-plugin="pageController">
                <div class="pages__layer pages__background js-pages-background" data-plugin="backgrounds"></div>
                <div class="pages__content js-pages-content">
                    <section class="works pages__layer" data-plugin="works">
                        <article class="view js-view">

                            <div class="view__screen work_content section_scroll view__screen--top view__screen--auto ui-light ui-light-background portfolio-bg">

                                <div class="row">
                                    <div class="inside_content banner_content">
                                        <h1 class="page_title white-color">
                                            IMA Gold<br>

                                            Gallery
                                        </h1>

                                        <p>Cover design, timeline graphics, engagement & audience building, <br>

                                            handling social accounts and covering all platforms.</p>
                                        <!-- <p>Show me:
                      <span style="color:#20a4cc" class="show_filter">Websites</span>
                    </p>
                    <ul class="filters js-works-filters inner-heading-content flex flex-align-start flex-justify-start">
                      <li>
                        <a data-id="work">Case Studies</a>
                      </li>
                      <li>
                        <a data-id="websites" class="is-active">Websites</a>
                      </li>
                      <li>
                        <a data-id="carryme">carryme</a>
                      </li>
                      <li>
                        <a data-id="coinsbars">coinsbars</a>
                      </li>
                      <li>
                        <a data-id="print">Branding & Identity</a>
                      </li>
                      <li>
                        <a data-id="mobile-apps">Mobile Apps</a>
                      </li>
                      <li>
                        <a data-id="social-media-marketing">Social Media Marketing</a>
                      </li>
                      <li>
                        <a data-id="advertisements">Advertisements</a>
                      </li>
                    </ul>-->
                                    </div>

                                </div>

                            </div>

                            <div class="portfolio-content-wrapper">
                                <div class="portfolio-content">
                                    <div class="portfolio-row">
                                        <div class="portfolio-column heading-column ">
                                            <div class="portfolio-heading animate-content text-left">IMA Gold<br>
                                                Shivajinagar
                                            </div>
                                            <a href="images/portfolio/social-media-marketing/photo-base/1.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/photo-base/1.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/photo-base/2.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/photo-base/2.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/photo-base/3.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/photo-base/3.jpg" class="animate-content">
                                            </a>

                                        </div>
                                        <div class="portfolio-column animate-content">
                                            <a href="images/portfolio/social-media-marketing/photo-base/4.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/photo-base/4.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/photo-base/5.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/photo-base/5.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/photo-base/6.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/photo-base/6.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/photo-base/7.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/photo-base/7.jpg" class="animate-content">
                                            </a>

                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="portfolio-row down-row">
                                        <div class="portfolio-column heading-column ">
                                            <div class="portfolio-heading animate-content text-left">IMA Gold <br>Yeshwantpur
                                            </div>
                                            <a href="images/portfolio/social-media-marketing/festival/1.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/festival/1.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/festival/2.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/festival/2.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/festival/3.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/festival/3.jpg" class="animate-content">
                                            </a>
                                        </div>
                                        <div class="portfolio-column animate-content">
                                            <a href="images/portfolio/social-media-marketing/festival/4.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/festival/4.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/festival/5.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/festival/5.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/festival/6.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/festival/6.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/festival/7.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/festival/7.jpg" class="animate-content">
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="portfolio-row down-row">
                                        <div class="portfolio-column heading-column ">
                                            <div class="portfolio-heading animate-content text-left">IMA Gold <br>
                                                Jayanagar
                                            </div>
                                            <a href="images/portfolio/social-media-marketing/text-base/2.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/text-base/2.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/text-base/3.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/text-base/3.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/text-base/4.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/text-base/4.jpg" class="animate-content">
                                            </a>

                                        </div>
                                        <div class="portfolio-column animate-content">
                                            <a href="images/portfolio/social-media-marketing/text-base/5.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/text-base/5.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/text-base/6.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/text-base/6.jpg" class="animate-content">
                                            </a>
                                            <a href="images/portfolio/social-media-marketing/text-base/7.jpg" data-group="gallery" class="portfolio-gallery">
                                                <img src="#" data-src="images/portfolio/social-media-marketing/text-base/7.jpg" class="animate-content">
                                            </a>

                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <!-- <div class="next-btn-sec">
                                    <a class="btn-1" href="dashboards.html">
                                        <svg>
                                            <rect x="0" y="0" fill="none" width="100%" height="100%"></rect>
                                        </svg>Next Service
                                    </a>
                                </div>-->
                            </div>





                        </article>

                    </section>
                </div>
            </div>
        </div>

    </main>

    <script src="js/shared.js"></script>
    <script>
        var STATE = {
            'ui': 'light',

            'common': {
                'visible': true,
                'staticHeader': true
            },
            'background': {
                'id': '',
                'effect': 0
            }
        };

    </script>
    <!-- <script src="js/works.js"></script> -->

    <script src="js/jquery.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/modaal.min.js"></script>
    <script src="js/jquery.unveil.js"></script>


    <script>
        jQuery(document).ready(function($) {
            $("img").unveil();
            $('.portfolio-gallery').modaal({
                type: 'image'
            });
            /*jQuery('html, body').animate({
              scrollTop: jQuery(jQuery('.portfolio-content-wrapper')).offset().top
            }, 1500, 'linear');*/

            jQuery('.show_filter').click(function(event) {
                // console.log(event);
                jQuery('.js-works-filters').toggleClass('show');
            });
            jQuery('.js-works-filters a').click(function(event) {
                jQuery('.js-works-filters').removeClass('show');
                var elem = event.currentTarget;
                var page = jQuery(elem).attr('data-id');
                if (page !== 'loan') {
                    window.location.href = page + ".html";
                }


            });
        });

    </script>
    <script>
        jQuery(document).ready(function($) {

            var h = window.innerHeight ||
                document.documentElement.clientHeight ||
                document.body.clientHeight;

            jQuery('.siteheader-toggle').click(function(event) {
                jQuery('.siteheader-menu').toggleClass('open_menu');
                if (jQuery('.siteheader-menu').hasClass('open_menu') == true) {
                    jQuery('.siteheader-menu').height(((h * 88) / 100).toFixed(0));
                } else {
                    jQuery('.siteheader-menu').height(65);
                }
            });

            jQuery('.works-item__title__link,.works-item__button').click(function(event) {
                alert('Coming Soon!');
                return false;
            });
        });

    </script>

</body>

</html>
