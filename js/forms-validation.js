$(document).ready(function() {
  var validator = $("#contact_form").validate({
    rules: {
      full_name: { required: true },
      email_address: { required: true, email: true },
      company_name: { required: true },
      message: { required: true },
      captcha: { required: true }
    },
    messages: {
      full_name: { required: "" },
      email_address: { required: "", email: "" },
      company_name: { required: "" },
      message: { required: "" },
      captcha: { required: "" }
    },

    // onfocusout: function(element, event) {
    //   console.log(element);
    // },
    // the errorPlacement has to take the table layout into account
    errorPlacement: function(error, element) {
      // $(element)
      //   .parent()
      //   .addClass("hasError");
      if (element.is(":radio"))
        error.appendTo(
          element
            .parent()
            .next()
            .next()
        );
      else if (element.is(":checkbox")) error.appendTo(element.next());
      else error.appendTo(element.parent());
    },

    submitHandler: function(form) {
      // alert("submitted!");

      form.submit();
    },

    // specifying a submitHandler prevents the default submit, good for the demo
    // set this class to error-labels to indicate valid fields
    success: function(label) {
      // set &nbsp; as text for IE
      label.html("").addClass("checked");
      //    form.submit();
    }
  });
});
